from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponse

from django.contrib.auth import authenticate
from django.contrib.auth import login as login_django


from forms import LoginForm
# Create your views here.
def show(request):
    return HttpResponse('Hola desde el cliente')

def login(request):
    message = None
    if request.method == "POST":
        username_post = request.POST['username']
        password_post = request.POST['password']

        user = authenticate(username = username_post, password = password_post)

        if user is not None:
            login_django(request, user)
            return redirect('clients:dashboard')
        else:
            message = "username y password incorrecto"

    form = LoginForm()
    context = {'form': form, 'message':message}
    return render(request, 'login.html',context)

def dashboard(request):
    return render(request, 'dashboard.html',{})
