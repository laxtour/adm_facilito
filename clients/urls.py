from django.conf.urls import url
from . import views

app_name = "clients"

urlpatterns = [
    # Examples:
   url(r'^show/$', views.show , name="show"),
   url(r'^login/$', views.login, name="login"),
   url(r'^dashboard/$', views.dashboard, name="dashboard"),

]
